package com.genesissolution.www.fileencppicker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AndroidExplorer extends Activity  implements AdapterView.OnItemClickListener{
	private static String key = "1111333399992222";

	private List<String> item = null;
	private List<String> path = null;
	private String root = "/sdcard";
	private TextView myPath,Paths,PathsE;
ListView lv;
	private static final String method = "AES";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.explorer);
		myPath = (TextView) findViewById(R.id.path);
		Paths = (TextView) findViewById(R.id.pathselected);
		PathsE = (TextView) findViewById(R.id.pathstatus);
		lv = (ListView) findViewById(R.id.listitem);
		key= this.getResources().getString(R.string.AES_encryption_Key);


		Button reset=(Button)findViewById(R.id.reset);
		reset.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Paths.setText("");
				PathsE.setText("");
				//cancel(true);

			}
		});
		Button b=(Button)findViewById(R.id.browse);
		b.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getDir(root);

			}
		});
		Button b1=(Button)findViewById(R.id.encrypt);
		b1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			String params=Paths.getText().toString().trim();
				int s=params.lastIndexOf("/");
				String substring=params.substring(s+1, params.length() - 1);
				String subdirstring=params.substring(0,s+1);
				String path=subdirstring+"E"+substring;

				Toast.makeText(AndroidExplorer.this, path, Toast.LENGTH_LONG).show();
				Toast.makeText(AndroidExplorer.this, key, Toast.LENGTH_SHORT).show();


				new Asyncencrypt(AndroidExplorer.this).execute(new String[]{Paths.getText().toString().trim()});

			}
		});
	}

	private void getDir(String dirPath) {
		myPath.setText("Location: " + dirPath);

		item = new ArrayList<String>();
		path = new ArrayList<String>();

		File f = new File(dirPath);
		File[] files = f.listFiles();

		if (!dirPath.equals(root)) {

			item.add(root);
			path.add(root);

			item.add("../");
			path.add(f.getParent());

		}

		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			path.add(file.getPath());
			if (file.isDirectory())
				item.add(file.getName() + "/");
			else
				item.add(file.getName());
		}

		ArrayAdapter<String> fileList = new ArrayAdapter<String>(this,
				R.layout.explorer_row, item);
		lv.setAdapter(fileList);
		lv.setOnItemClickListener(this);
		//setListAdapter(fileList);
	}

	File file;

	//@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		file = new File(path.get(position));

		if (file.isDirectory()) {
			if (file.canRead())
				getDir(path.get(position));
			else {
				new AlertDialog.Builder(this)
						.setIcon(R.drawable.icon)
						.setTitle(
								"[" + file.getName()
										+ "] folder can't be read!")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
									}
								}).show();
			}
		} else {
			new AlertDialog.Builder(this)
					.setIcon(R.drawable.icon)
					.setTitle("Select")
					.setMessage("Select " + file.getName() + "to server ?")
					.setPositiveButton("Select",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Toast.makeText(
											AndroidExplorer.this,
											"" + file.getAbsolutePath()
													+ " iss selected ", Toast.LENGTH_LONG)
											.show();

									Paths.setText(""+file.getAbsolutePath());
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							}).show();
		}
	}


	class  Asyncencrypt extends AsyncTask<String,Void,String>
	{
		Context c;
		Asyncencrypt(Context c)
		{
			this.c=c;
		}

		@Override
		protected String doInBackground(String... params) {
			try {
			File inFile = new File(params[0].toString());

				int s=params[0].lastIndexOf("/");
				String substring=params[0].substring(s + 1, params[0].length());
				String subdirstring=params[0].substring(0, s + 1);
				String path=subdirstring+"E"+substring;


				File outFile = new File(path);

                if(!outFile.exists())
				{
					outFile.createNewFile();
				}


				encrypt(new FileInputStream(inFile), new FileOutputStream(outFile));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				return e.getMessage();
			}
			catch (Exception e1) {
				// TODO Auto-generated catch block
			//	e1.printStackTrace();
				return e1.getMessage();
			}
return "success";
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);
			Toast.makeText(c, s, Toast.LENGTH_LONG).show();
		//	Toast.makeText(c, outFile, Toast.LENGTH_LONG).show();
			PathsE.setText("Encrypted");

		}
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		file = new File(path.get(position));

		if (file.isDirectory()) {
			if (file.canRead())
				getDir(path.get(position));
			else {
				new AlertDialog.Builder(this)
						.setIcon(R.drawable.icon)
						.setTitle(
								"[" + file.getName()
										+ "] folder can't be read!")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
														int which) {
									}
								}).show();
			}
		} else {
			new AlertDialog.Builder(this)
					.setIcon(R.drawable.icon)
					.setTitle("Select")
					.setMessage("Select " + file.getName() + "to server ?")
					.setPositiveButton("Select",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
													int which) {
									Toast.makeText(
											AndroidExplorer.this,
											"" + file.getAbsolutePath()
													+ " iss selected ", Toast.LENGTH_LONG)
											.show();

									Paths.setText(""+file.getAbsolutePath());
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
													int which) {
									dialog.dismiss();
								}
							}).show();
		}
	}


	public static SecretKeySpec keyGen(String s) throws NoSuchAlgorithmException
	{
		KeyGenerator kgen = KeyGenerator.getInstance(method);
		kgen.init(128); // 192 and 256 bits may not be available
		byte[] raw = s.getBytes();
		SecretKeySpec result = new SecretKeySpec(raw, method);
		//System.out.println(�result �+new String(result.getEncoded()));
		// Instantiate the cipher
		return result;
	}

	public static void encrypt(InputStream is, OutputStream os)
	{
		encryptOrDecrypt(Cipher.ENCRYPT_MODE, is, os);
	}

	public static void decrypt(InputStream is, OutputStream os)
	{
		encryptOrDecrypt(Cipher.DECRYPT_MODE, is, os);
	}

	public static void encryptOrDecrypt(int mode, InputStream is, OutputStream os)
	{
		try
		{
			Cipher cipher = Cipher.getInstance(method); // DES/ECB/PKCS5Padding for SunJCE
			if (mode == Cipher.ENCRYPT_MODE)
			{
				cipher.init(Cipher.ENCRYPT_MODE, keyGen(key));
				CipherInputStream cis = new CipherInputStream(is, cipher);

				IOUtils.copy(cis, os);


				//doCopy(cis, os);
			} else if (mode == Cipher.DECRYPT_MODE)
			{
				cipher.init(Cipher.DECRYPT_MODE, keyGen(key));
				CipherOutputStream cos = new CipherOutputStream(os, cipher);

				IOUtils.copy(is, cos);
				//doCopy(is, cos);
			}
		} catch (Exception e)
		{
			e.printStackTrace();

		} catch (Throwable e)
		{
			e.printStackTrace();
		}
	}

	public static void doCopy(InputStream is, OutputStream os) throws IOException
	{
    /* byte[] bytes = new byte[1024];
    int numBytes;
    while ((numBytes = is.read(bytes)) != -1) {
    os.write(bytes, 0, numBytes);
    }
    os.flush();
    os.close();
    is.close();*/

		byte[] buffer = new byte[8192]; // Or larger (but use powers of 2)
		int bytesRead;
		while ((bytesRead = is.read(buffer)) != -1)
		{
			os.write(buffer, 0, bytesRead);
		}
		os.flush();
		os.close();
		is.close();
	}


	public static SecretKey generateKey(char[] passphraseOrPin, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
		// Number of PBKDF2 hardening rounds to use. Larger values increase
		// <span id="IL_AD9" class="IL_AD">computation</span> time. You should select a value that causes computation
		// to take >100ms.
		final int iterations = 1000;

		// Generate a 256-bit key
		final int outputKeyLength = 256;

		SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec keySpec = new PBEKeySpec(passphraseOrPin, salt, iterations, outputKeyLength);
		SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
		return secretKey;
	}

}